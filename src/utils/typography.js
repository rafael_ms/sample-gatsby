import Typography from "typography";

// const typography = new Typography({
//   baseFontSize: "16px",
//   baseLineHeight: 1.45,
//   headerFontFamily: [
//     "Avenir Next",
//     "Helvetica Neue",
//     "Segoe UI",
//     "Helvetica",
//     "Arial",
//     "sans-serif",
//   ],
//   bodyFontFamily: ["Georgia", "serif"],
// });

import bootstrapTheme from "typography-theme-bootstrap";
import lawtonTheme from "typography-theme-lawton";
import fairyGateTheme from "typography-theme-fairy-gates";
import kirkhamTheme from "typography-theme-kirkham";

const typography = new Typography(kirkhamTheme);

export default typography;