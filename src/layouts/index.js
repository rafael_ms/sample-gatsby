import React from "react";
import Link from "gatsby-link";
import g from "glamorous";
import { css } from "glamor";

import { rhythm } from "../utils/typography";

const linkStyle = css({ float: `right` });

const ListLink = props =>
  <li style={{ display: `inline-block`, marginRight: `1rem` }}>
    <Link to={props.to}>
      {props.children}
    </Link>
  </li>

export default ({ children, data }) =>
//   <div style={{ margin: `0 auto`, maxWidth: 650, padding: `1.25rem 1rem` }}>
//     <header style={{ marginBottom: `1.5rem` }}>
//       <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
//         <h3 style={{ display: `inline` }}>MySweetSite</h3>
//       </Link>
//       <ul style={{ listStyle: `none`, float: `right` }}>
//         <ListLink to="/">Home</ListLink>
//         <ListLink to="/about/">About</ListLink>
//         <ListLink to="/contact/">Contact</ListLink>
//         <ListLink to="/page-2/">Page 2</ListLink>
//         <ListLink to="/counter/">Counter</ListLink>
//       </ul>
//     </header>
    <g.Div
        margin={`0 auto`}
        maxWidth={700}
        padding={rhythm(2)}
        paddingTop={rhythm(1.5)}
    >
    <header style={{ marginBottom: `1.5rem` }}>
        <Link to={`/`}>
        <g.H3
            marginBottom={rhythm(2)}
            display={`inline-block`}
            fontStyle={`normal`}
        >
            {data.site.siteMetadata.title}
        </g.H3>
        </Link>
        <ul style={{ listStyle: `none`, float: `right`, fontSize: `12px` }}>
            <ListLink to="/">Home</ListLink>
            <ListLink to="/about/">About</ListLink>
            <ListLink to="/contact/">Contact</ListLink>
            <ListLink to="/counter/">Counter</ListLink>
            <ListLink to="/my-files/">Files</ListLink>
        </ul>
    </header>
    {children()}
    </g.Div>
    //   </div>
export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`